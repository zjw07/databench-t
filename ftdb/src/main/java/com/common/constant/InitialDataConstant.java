/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.common.constant;

/**
 * 账户配置常量
 *
 */
public class InitialDataConstant {
	/**
	 * 初始化地址常量
	 */
	public static final String INITIALIZATION_ADDRESS = "北京市海淀区中关村";
	/**
	 * 初始化固定电话
	 */
	public static final String INITIALIZATION_PHONE = "010-56872293";


}
