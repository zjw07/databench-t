/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.common.constant;
/**
 * 交易配置常量
 *
 */
public class TrancfgConstant {
	/**
	 * 转账业务
	 */
	public static final String TRANCFG_ACCOUNTS_TRANSFER = "1001";
	/**
	 * 账户查询
	 */
	public static final String TRANCFG_ACCOUNTS_QUERY = "1002";
	/**
	 * 代发工资
	 */
	public static final String TRANCFG_ACCOUNTS_SALARY = "1003";
	/**
	 * 存款
	 */
	public static final String TRANCFG_ACCOUNTS_DEPOSIT = "1004";
	/**
	 * 取款
	 */
	public static final String TRANCFG_ACCOUNTS_WITHDRAW = "1005";
	/**
	 * 资产盘点
	 */
	public static final String TRANCFG_ACCOUNTS_INVENTORY = "1006";

}
