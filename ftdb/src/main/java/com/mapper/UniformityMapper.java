/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.mapper;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.pojo.ChkuplogCount;
import com.pojo.Tranlist;
import com.pojo.TranlistCount;
import com.pojo.Tranlog;
import com.pojo.TranlogCount;

@Mapper
@Repository
public interface UniformityMapper {

	BigDecimal sumAccount();
	
	Integer tranlogBranchCount();
	
	List<TranlistCount> sumTranlistCount();
	
	Integer tranlogCount(Tranlog tranlog);

	Integer tranlistCount(Tranlist tranlist);

	List<TranlogCount> tranlogCostTimeCount(String tranlog_fld1);
	
	List<ChkuplogCount> chkuplogCount();
	
	List<String> accountInventory();

	Integer tranlogHistoryBranchCount();

	BigDecimal sumIncressAccount(String tranlist_fld2);
	
}
