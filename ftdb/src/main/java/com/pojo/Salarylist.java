/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

import java.math.BigDecimal;

public class Salarylist {

    private String salarylist_branchid;
    private String salarylist_accountid;
    private BigDecimal salarylist_bale;
    private String salarylist_fld1; 
    private String salarylist_fld2;
    private BigDecimal  salarylist_fld3; 
    private BigDecimal  salarylist_fld4;
    public String getSalarylist_branchid() {
        return salarylist_branchid;
    }

    public void setSalarylist_branchid(String salarylist_branchid) {
        this.salarylist_branchid = salarylist_branchid;
    }

    public String getSalarylist_accountid() {
        return salarylist_accountid;
    }

    public void setSalarylist_accountid(String salarylist_accountid) {
        this.salarylist_accountid = salarylist_accountid;
    }

    public BigDecimal getSalarylist_bale() {
        return salarylist_bale;
    }

    public void setSalarylist_bale(BigDecimal salarylist_bale) {
        this.salarylist_bale = salarylist_bale;
    }

	public String getSalarylist_fld1() {
		return salarylist_fld1;
	}

	public void setSalarylist_fld1(String salarylist_fld1) {
		this.salarylist_fld1 = salarylist_fld1;
	}

	public String getSalarylist_fld2() {
		return salarylist_fld2;
	}

	public void setSalarylist_fld2(String salarylist_fld2) {
		this.salarylist_fld2 = salarylist_fld2;
	}

	public BigDecimal getSalarylist_fld3() {
		return salarylist_fld3;
	}

	public void setSalarylist_fld3(BigDecimal salarylist_fld3) {
		this.salarylist_fld3 = salarylist_fld3;
	}

	public BigDecimal getSalarylist_fld4() {
		return salarylist_fld4;
	}

	public void setSalarylist_fld4(BigDecimal salarylist_fld4) {
		this.salarylist_fld4 = salarylist_fld4;
	}
    
    
}
