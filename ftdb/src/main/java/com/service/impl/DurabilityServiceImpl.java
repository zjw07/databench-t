/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;

import com.common.constant.AccountConstant;
import com.common.constant.ChkuplogConstant;
import com.localMapper.LocalTranLogMapper;
import com.mapper.ChkuplogMapper;
import com.mapper.UniformityMapper;
import com.pojo.Chkuplog;
import com.pojo.Tranlog;
import com.service.DurabilityService;

@Service
@Scope("prototype")
public class DurabilityServiceImpl extends Thread implements DurabilityService{
    @Autowired
    private ChkuplogMapper chkuplogMapper;
    @Autowired
    DataSourceTransactionManager dataSourceTransactionManager;
    @Autowired
    TransactionDefinition transactionDefinition;
    @Autowired
    private UniformityMapper uniformityMapper;    
    @Autowired
    private LocalTranLogMapper localTranLogMapper; 
    
    private final Logger logger = LoggerFactory.getLogger(DurabilityServiceImpl.class); 
    
    @Override
	public void run() {
    	logger.info("持久性检测线程启动了");
    	Tranlog tranlogSalary = new Tranlog();
    	Integer mainTranLogCount = uniformityMapper.tranlogCount(tranlogSalary);
    	if(mainTranLogCount==null) {
    		mainTranLogCount = 0;
    	}
    	Tranlog param = new Tranlog();
    	Long localTranLogCount = localTranLogMapper.countTranlog(param);
    	if(localTranLogCount==null) {
    		localTranLogCount = 0l;
    	}
    	if(mainTranLogCount.intValue()==localTranLogCount.intValue()) {
    		//如果业务库与本地库tranlog表数量一致，则持久性测试通过
    		logger.info(" DurabilityServiceImpl Durability success ");
        	insertChkuplog(new Chkuplog(ChkuplogConstant.CHKUPLOG_TYPE_D,System.currentTimeMillis()+"",AccountConstant.ACCOUNT_TRAN_SUCCESS,"DurabilityServiceImpl Durability success "));
    	}else {
    		logger.info(" DurabilityServiceImpl Durability failure mainTranLogCount {} localTranLogCount {} ",mainTranLogCount,localTranLogCount);
        	insertChkuplog(new Chkuplog(ChkuplogConstant.CHKUPLOG_TYPE_D,System.currentTimeMillis()+"",AccountConstant.ACCOUNT_TRAN_FAILURE,"DurabilityServiceImpl Durability failure "+mainTranLogCount+" "+localTranLogCount));
    	}
    	logger.info("持久性检测线程结束了");
	}
    
	/**
	 * 插入列表
	 * @param chkuplog
	 */
    public void insertChkuplog(Chkuplog chkuplog) {
    	TransactionStatus transactionStatus = null;
    	try {
    		transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
    		chkuplogMapper.insertChkuplog(chkuplog);
    		logger.info(" DurabilityServiceImpl insertChkuplog  success ");
        	dataSourceTransactionManager.commit(transactionStatus);//提交
		} catch (Exception e) {
			logger.error(" DurabilityServiceImpl insertChkuplog  rollback e {}" ,e);
	    	dataSourceTransactionManager.rollback(transactionStatus);
		}
	}

}