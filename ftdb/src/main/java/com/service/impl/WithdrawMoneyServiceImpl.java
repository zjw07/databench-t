/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.service.impl;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;

import com.common.calculation.AmountUtil;
import com.common.calculation.InitiUtil;
import com.common.constant.AccountConstant;
import com.common.constant.PropertiesConstant;
import com.common.context.SpringContextUtils;
import com.mapper.Transfer;
import com.pojo.Account;
import com.pojo.Branch;
import com.pojo.Customer;
import com.pojo.Sjno;
import com.pojo.Trancfg;
import com.pojo.Tranlist;
import com.pojo.Tranlog;
import com.service.WithdrawMoneyService;


@Service
@Scope("prototype")
public class WithdrawMoneyServiceImpl implements WithdrawMoneyService,Callable<Long> {

    private final Logger logger = LoggerFactory.getLogger(WithdrawMoneyServiceImpl.class); 

    @Autowired
    DataSourceTransactionManager dataSourceTransactionManager;
    @Autowired
    TransactionDefinition transactionDefinition;
    @Autowired
    private PropertiesConstant propertiesConstant;
    @Autowired
    private Transfer transfer;

    private int num;
    
    private int datacfg_accnum;
    private String process_id;

    private String trancfgType;
    private Trancfg tf;

    /**
     * 单线程执行取款
     * @param num
     * @param datacfg_accnum
     * @throws Exception 
     */
    public Long call() throws Exception {
    	long countTime = System.currentTimeMillis();
        for (int i = 0; i < num; i++) {
        	Date nowDate = new Date();
        	long nowMillis = nowDate.getTime();
    		Time nowTime = new Time(nowMillis);
    		BigDecimal money = AmountUtil.getRandomMoney(0,AccountConstant.ACCOUNT_DEFAULT_MONERY);//模拟存款金额为
            String accountFrom = "60"+String.valueOf((new Random().nextInt(datacfg_accnum)+1)+InitiUtil.getAccnum(datacfg_accnum));//借方
    		Account from = null;
    		try {
        		from = transfer.readAccount(accountFrom);
			} catch (Exception e) {
				e.printStackTrace();
    			logger.error("WithdrawMoneyServiceImpl deposit readAccount error {} ",e);
			}
    		insertBreakPoint();
			if(from==null) {
				logger.info("WithdrawMoneyServiceImpl deposit Account from {} is null ", accountFrom);
				continue;
			}
			Tranlog tranLog = null;
          	int fromSeq = updateAccountBranchSeq(from);
          	if(fromSeq==AccountConstant.ACCOUNT_SEQ_FAILURE) {//更新借方网点流水号失败
          		tranLog = new Tranlog(nowDate,from.getAccount_branchid(),fromSeq,trancfgType
            			,nowDate.getTime(),System.currentTimeMillis(),(int)(System.currentTimeMillis()-nowMillis),AccountConstant.ACCOUNT_TRAN_FAILURE,"",process_id);
    			logger.error("WithdrawMoneyServiceImpl deposit updateAccountBranchSeq from {} branch {} seq {} rollback ", accountFrom,from.getAccount_branchid(),fromSeq);
	          	continue;
          	}else {
    			logger.info("WithdrawMoneyServiceImpl deposit updateAccountBranchSeq from {} branch {} seq {} success ", accountFrom,from.getAccount_branchid(),fromSeq);
          	}
          	try {
          		if((from.getAccount_stat().equals(AccountConstant.ACCOUNT_NORMAL)
        				||from.getAccount_stat().equals(AccountConstant.ACCOUNT_ONLYOUT))) {
        			tranLog = accessMoney(accountFrom,money,tranLog,fromSeq,nowDate,nowTime,nowMillis);//更新转账金额
        			if(tranLog==null) {//转账业务回滚
        				tranLog = new Tranlog(nowDate,from.getAccount_branchid(),fromSeq,trancfgType
                    			,nowDate.getTime(),System.currentTimeMillis(),(int)(System.currentTimeMillis()-nowMillis),AccountConstant.ACCOUNT_TRAN_FAILURE,"",process_id);
        	    		logger.error("WithdrawMoneyServiceImpl deposit rollback accountFrom {}" 
        						,accountFrom);
        			}
                }else {//借方账户不正常
                	tranLog = new Tranlog(nowDate,from.getAccount_branchid(),fromSeq,trancfgType
                			,nowDate.getTime(),System.currentTimeMillis(),(int)(System.currentTimeMillis()-nowMillis),AccountConstant.ACCOUNT_TRAN_FAILURE,"",process_id);
        			logger.info("WithdrawMoneyServiceImpl deposit Account from {} not normal state is {} " ,accountFrom, from.getAccount_stat() );
                }
			} catch (Exception e) {
				logger.error("DepositMoneyServiceImpl updateAccountBranchSeq run error brachid {} e {}" 
						,from.getAccount_branchid(),e);		
			}
            //插入transLog
          	insertAccountTranlog(tranLog,0);
        }
        return System.currentTimeMillis()-countTime;
	}

    /**
     * 插入断点
     */
    private void insertBreakPoint() {
    	if(tf!=null&&"Y".equals(tf.getTrancfg_brkpot())) {
    		try {
				Thread.sleep(Long.parseLong(tf.getTrancfg_brktime()));
			} catch (Exception e) {
		    	logger.error("WithdrawMoneyServiceImpl insertBreakPoint is run error e {} ",e);
			}
    	}
	}

	/**
     * 插入tranLog
     * @param tranLog
     */
    public void insertAccountTranlog(Tranlog tranLog,int repeat) {
    	TransactionStatus transactionStatus = null;
    	boolean isSuccess = false;
    	try {
    		transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
    		transfer.insertTranlog(tranLog);
    		logger.info("WithdrawMoneyServiceImpl insertAccountTranlog tranlog_date {} tranlog_branchid {} tranlog_branchseq {} success "
    				,tranLog.getTranlog_date(),tranLog.getTranlog_branchid(),tranLog.getTranlog_branchseq());
    		insertBreakPoint();
        	dataSourceTransactionManager.commit(transactionStatus);//提交
        	isSuccess = true;
		} catch (Exception e) {
			logger.error("WithdrawMoneyServiceImpl insertAccountTranlog rollback tranlog_date {} tranlog_branchid {} tranlog_branchseq {} e {}" 
					,tranLog.getTranlog_date(),tranLog.getTranlog_branchid(),tranLog.getTranlog_branchseq(),e);
	    	try {
				dataSourceTransactionManager.rollback(transactionStatus);
			} catch (Exception e2) {
				logger.error("WithdrawMoneyServiceImpl insertAccountTranlog rollback error tranlog_date {} tranlog_branchid {} tranlog_branchseq {} e {}" 
						,tranLog.getTranlog_date(),tranLog.getTranlog_branchid(),tranLog.getTranlog_branchseq(),e);
			}
		}
    	if(isSuccess) {
    		LocalTranLogServiceImpl localTranLogServiceImpl = SpringContextUtils.getBean(LocalTranLogServiceImpl.class);
        	localTranLogServiceImpl.setTranlog(tranLog);
        	localTranLogServiceImpl.start();
    	}else {
			logger.error("========>重试次数"+repeat+"===>"+tranLog.getTranlog_branchid());
    		if(repeat<propertiesConstant.getRepeat()) {
    			try {
					Thread.sleep(500);
	    	 		insertAccountTranlog(tranLog,repeat+1);
				} catch (InterruptedException e) {
					logger.error("DepositMoneyServiceImpl insertAccountTranlog Thread error tranlog_date {} tranlog_branchid {} tranlog_branchseq {} e {}" 
							,tranLog.getTranlog_date(),tranLog.getTranlog_branchid(),tranLog.getTranlog_branchseq(),e);
				}
    		}
    	}
	}

	/**
     * 更新网点流水号
     * @param account
     */
    public int updateAccountBranchSeq(Account account) {
    	TransactionStatus transactionStatus = null;
    	int seq = AccountConstant.ACCOUNT_SEQ_FAILURE;
    	try {
    		transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
    		Branch branch = transfer.readTransactionalBranchSeq(account.getAccount_branchid());
        	seq = branch.getBranch_seq();
    		transfer.updateBranch(branch.getBranch_id());//更新网点流水号
    		insertBreakPoint();
        	dataSourceTransactionManager.commit(transactionStatus);//提交
		} catch (Exception e) {
			e.printStackTrace();
			try {
		    	dataSourceTransactionManager.rollback(transactionStatus);
			} catch (Exception e2) {
				logger.error("WithdrawMoneyServiceImpl updateAccountBranchSeq rollback error account {} branchId {} e {}",account.getAccount_id(),account.getAccount_branchid(),e);
			}
	    	logger.error("WithdrawMoneyServiceImpl updateAccountBranchSeq rollback account {} branchId {} e {} " 
					,account.getAccount_id(),account.getAccount_branchid(),e);
	    	return AccountConstant.ACCOUNT_SEQ_FAILURE;

		}
		return seq;
	}

	/**
     * 更新转账金额
     * @param accountFrom
     * @param money
     * @param tranLog
	 * @param toSeq 
	 * @param fromSeq 
	 * @param nowTime 
	 * @param nowDate 
	 * @param nowMillis 
	 * @param branchIdList 
	 * @return 
     */
    public Tranlog accessMoney(String accountFrom, BigDecimal money, Tranlog tranLog, int fromSeq, Date nowDate, Time nowTime, long nowMillis) {
    	TransactionStatus transactionStatus = null;
    	try {
    		transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
    		Account from = transfer.readTransactionalAccount(accountFrom);
    		if(from.getAccount_bale().subtract(money).doubleValue()<0) {
    			logger.info("WithdrawMoneyServiceImpl accessMoney Account from  {} money {} is not enough {} ",accountFrom, from.getAccount_bale(),money );
    			tranLog = new Tranlog(nowDate,from.getAccount_branchid(),fromSeq,trancfgType
            			,nowDate.getTime(),System.currentTimeMillis(),(int)(System.currentTimeMillis()-nowMillis),AccountConstant.ACCOUNT_TRAN_FAILURE,"",process_id);
    		}else {
        		//更新借方余额和借方科目余额
        		Account af = new Account();
                af.setAccount_id(accountFrom);
                af.setAccount_bale(money);
                transfer.transferFrom(af);
                Sjno sf = new Sjno();
                sf.setSjno_id(from.getAccount_sjnoid());
                sf.setSjno_branchid(from.getAccount_branchid());
                sf.setSjno_bale(money);
                transfer.transferFromSjno(sf);
        		logger.info("WithdrawMoneyServiceImpl accessMoney update accountSjno from {} Sjon {} monney {} success ", accountFrom,from.getAccount_sjnoid(),money);
        		Customer customerfrom = new Customer();
        		customerfrom.setCustomer_id(from.getAccount_custid());
        		customerfrom.setCustomer_bale(money);
        		transfer.updateCustomerFrom(customerfrom);
        		logger.info("WithdrawMoneyServiceImpl accessMoney update customerfrom {} monney {} success ", from.getAccount_custid(),money);
        		//插入tranlist记录
        		Tranlist fromTranList = new Tranlist(nowDate,from.getAccount_branchid(), 
                		fromSeq, nowTime,from.getAccount_id(), money,
                		AccountConstant.TRANLIST_D, from.getAccount_id(),trancfgType,process_id,money.negate());//写借方tranlist
                transfer.insertTranlist(fromTranList);
                Tranlist toTranList = new Tranlist(nowDate,from.getAccount_branchid(),
                		fromSeq,nowTime,from.getAccount_id(),money,
                		AccountConstant.TRANLIST_C,from.getAccount_id(),trancfgType,process_id,new BigDecimal(0));//写贷方tranlist
                transfer.insertTranlist(toTranList);
        		logger.info("WithdrawMoneyServiceImpl accessMoney insert to tranlist account {} success ", accountFrom);
            	tranLog = new Tranlog(nowDate,from.getAccount_branchid(),fromSeq,trancfgType
            			,nowDate.getTime(),System.currentTimeMillis(),(int)(System.currentTimeMillis()-nowMillis),AccountConstant.ACCOUNT_TRAN_SUCCESS,"",process_id);
    		}
    		insertBreakPoint();
        	dataSourceTransactionManager.commit(transactionStatus);//提交
		} catch (Exception e) {
			logger.error("WithdrawMoneyServiceImpl accessMoney rollback accountFrom {} e {}" 
					,accountFrom,e);
			try {
				dataSourceTransactionManager.rollback(transactionStatus);
			} catch (Exception e2) {
				logger.error("WithdrawMoneyServiceImpl accessMoney rollback error accountTo {} e {}",e);
			}
	    	return null;
		}
    	return tranLog;

	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getDatacfg_accnum() {
		return datacfg_accnum;
	}

	public void setDatacfg_accnum(int datacfg_accnum) {
		this.datacfg_accnum = datacfg_accnum;
	}

	public String getTrancfgType() {
		return trancfgType;
	}

	public void setTrancfgType(String trancfgType) {
		this.trancfgType = trancfgType;
	}

	public String getProcess_id() {
		return process_id;
	}

	public void setProcess_id(String process_id) {
		this.process_id = process_id;
	}

	public Trancfg getTf() {
		return tf;
	}

	public void setTf(Trancfg tf) {
		this.tf = tf;
	}
	
	
}
